package domain;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Order {
    private int orderNr;
    private boolean isStudentOrder;

    private ArrayList<MovieTicket> tickets;

    public Order(int orderNr, boolean isStudentOrder) {
        this.orderNr = orderNr;
        this.isStudentOrder = isStudentOrder;

        tickets = new ArrayList<MovieTicket>();
    }

    public int getOrderNr() {
        return orderNr;
    }

    public void addSeatReservation(MovieTicket ticket) {
        tickets.add(ticket);
    }

    public double calculatePrice() {
        if (tickets.size() == 0) return 0;
        double totalPrice = 0;
        int premiumTickets = 0;

        for (MovieTicket ticket : tickets) {
            if (ticket.isPremiumTicket()) premiumTickets++;
            totalPrice += ticket.getPrice();

        }

        //Handle 2nd ticket free (student or weekday)
        if (this.isStudentOrder || !isWeekend(LocalDateTime.now())) { //Every 2nd ticket free
            if (tickets.size() % 2 == 0) { //If even amount, easy way
                totalPrice = totalPrice / 2;
            } else { //If uneven amount, hard way
                double priceAvg = totalPrice / tickets.size(); //Calculate average price
                totalPrice = ((tickets.size() - 1) / 2) * priceAvg; //Remove 1 to get even number, divide by 2, times the average price
                totalPrice += priceAvg; //And add the uneven ticket back
            }
            if (premiumTickets % 2 == 0) {
                premiumTickets = premiumTickets / 2;
            } else {
                premiumTickets = (premiumTickets - 1) / 2; //Calculate the hard way like with the tickets
                premiumTickets++;
            }
        }

        totalPrice += (this.isStudentOrder) ? premiumTickets * 2 : premiumTickets * 3; //If student, add 2eu/premium ticket. Else add 3/premium ticket

        if (tickets.size() > 5 && isWeekend(LocalDateTime.now()) && !this.isStudentOrder) { //Group discount 10%
            totalPrice = totalPrice * 0.9;
        }

        return totalPrice;
    }

    public boolean isWeekend(LocalDateTime dt) {
        if (dt.getDayOfWeek() == DayOfWeek.FRIDAY ||
                LocalDateTime.now().getDayOfWeek() == DayOfWeek.SATURDAY ||
                LocalDateTime.now().getDayOfWeek() == DayOfWeek.SUNDAY) {
            return true;
        }
        return false;
    }

    public void export(TicketExportFormat exportFormat) {
        // Bases on the string respresentations of the tickets (toString), write
        // the ticket to a file with naming convention Order_<orderNr>.txt of
        // Order_<orderNr>.json
        try {
            if (exportFormat.equals(TicketExportFormat.PLAINTEXT)) {
                FileWriter plainTicket = new FileWriter("Order_" + String.valueOf(getOrderNr()) + ".txt");
                tickets.forEach(x -> {
                    try {
                        plainTicket.write(x.toString() + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                plainTicket.close();
            } else if (exportFormat.equals(TicketExportFormat.JSON)) {
                FileWriter plainTicket = new FileWriter("Order_" + String.valueOf(getOrderNr()) + ".json");
                plainTicket.write("[ \n");
                tickets.forEach(x -> {
                    try {
                        plainTicket.write('"' + x.toString() + '"' +",\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                plainTicket.write('"' + '"' +  "]");
                plainTicket.close();
            }

        } catch (IOException e) {
            System.out.println("Error");
            e.printStackTrace();
        }
    }
}
