package domain;

import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {
        Movie mov1 = new Movie("The Calculator");
        MovieScreening screening1 = new MovieScreening(mov1, LocalDateTime.now(), 10.00);
        MovieTicket ticket1 = new MovieTicket(screening1, true, 1, 2);
        MovieTicket ticket2 = new MovieTicket(screening1, true, 1, 3);

        Order order = new Order(1, false);
        order.addSeatReservation(ticket1);
        order.addSeatReservation(ticket2);
        System.out.println(order.calculatePrice());

        order.export(TicketExportFormat.PLAINTEXT);
        order.export(TicketExportFormat.JSON);
    }
}
