import domain.Movie;
import domain.MovieScreening;
import domain.MovieTicket;
import domain.Order;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;

public class TicketTest {

    private Movie movie;
    private MovieScreening movieScreening;

    @BeforeEach
    public void setUp() throws Exception {
        movie = new Movie("Inception");
        movieScreening = new MovieScreening(movie, LocalDateTime.now(), 10.00);
    }

    @Test
    @DisplayName("A Second ticket is free for students (Everyday of the week)")
    public void testStudentTicket() {
        MovieTicket ticket1 = new MovieTicket(movieScreening, false, 1, 2);
        MovieTicket ticket2 = new MovieTicket(movieScreening, false, 1, 3);
        Order order = new Order(1, true);

        order.addSeatReservation(ticket1);
        order.addSeatReservation(ticket2);

        assertEquals(10, order.calculatePrice());
    }

    @Test
    @DisplayName("A Second ticket is free Midweek for everyone")
    public void testSecondTicketFreeMidweekForEveryone() {
        movieScreening = new MovieScreening(movie, LocalDateTime.parse("2021-02-04T17:48:23.558"), 10.00);
        MovieTicket ticket1 = new MovieTicket(movieScreening, false, 1, 2);
        MovieTicket ticket2 = new MovieTicket(movieScreening, false, 1, 3);
        Order order = new Order(1, false);

        order.addSeatReservation(ticket1);
        order.addSeatReservation(ticket2);

        assertEquals(10, order.calculatePrice());
    }

    @Test
    @DisplayName("In the weeknd 6+ tickets achieves 10% discount")
    public void discountForSixOrMoreTicketsDuringTheWeekend() {
        MovieTicket ticket1 = new MovieTicket(movieScreening, false, 1, 2);
        MovieTicket ticket2 = new MovieTicket(movieScreening, false, 1, 3);
        MovieTicket ticket3 = new MovieTicket(movieScreening, false, 1, 4);
        MovieTicket ticket4 = new MovieTicket(movieScreening, false, 1, 5);
        MovieTicket ticket5 = new MovieTicket(movieScreening, false, 1, 6);
        MovieTicket ticket6 = new MovieTicket(movieScreening, false, 1, 7);
        Order order = new Order(1, false);

        order.addSeatReservation(ticket1);
        order.addSeatReservation(ticket2);
        order.addSeatReservation(ticket3);
        order.addSeatReservation(ticket4);
        order.addSeatReservation(ticket5);
        order.addSeatReservation(ticket6);

        if (order.isWeekend(LocalDateTime.now())) {
            // Result if today is the weekend
            assertEquals(54, order.calculatePrice());
        } else {
            // Result if today is midweek
            assertEquals(30, order.calculatePrice());
        }
    }

    @Test
    @DisplayName("Different prices for premium tickets")
    public void extraCostsForPremiumTickets() {
        MovieTicket ticket1 = new MovieTicket(movieScreening, true, 1, 2);
        Order studentOrder = new Order(1, true);
        Order normalOrder = new Order(2, false);

        studentOrder.addSeatReservation(ticket1);
        normalOrder.addSeatReservation(ticket1);

        // Result for studentOrder
        assertEquals(12, studentOrder.calculatePrice());
        // Result for normalOrder
        assertEquals(13, normalOrder.calculatePrice());
    }


    @Test
    @DisplayName("Second Ticket is still free with a premium ticket")
    public void secondTicketStillFreePremiumTicket() {
        MovieTicket ticket1 = new MovieTicket(movieScreening, true, 1, 2);
        MovieTicket ticket2 = new MovieTicket(movieScreening, true, 1, 3);

        Order order = new Order(1, true);

        order.addSeatReservation(ticket1);
        order.addSeatReservation(ticket2);

        // Result for order
        assertEquals(12, order.calculatePrice());
    }
}
